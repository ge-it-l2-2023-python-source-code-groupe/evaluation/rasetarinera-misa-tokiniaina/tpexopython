from tools.console import clear
def choix_exo_variables():
    clear()
    pass
    print(f'''
                
    {'_'*30}
    Chapitres Variables
    -------------------
    Selectionner l'exo :
                
    1 : 2_11_1
    2 : 2_11_2
    3 : 2_11_3
                
    ''')
    exo = input("veuillez selectionner un exo (q pour revenin): ")
    if(exo == '1'):
        from chapitres.variables.exo2_11_1 import run
        run()
    elif(exo == '2'):
        from chapitres.variables.exo2_11_2 import run
        run()
    elif(exo == '3'):
        from chapitres.variables.exo2_11_3 import run
        run()
    elif exo == 'q':
        return
    else:
        print("invalide")
        
    precedent = input(f'''
{'_'*30}

allez dans menu ? [y/n]--> ''')
    if(precedent != 'y'):
        choix_exo_variables()
    

def choix_exo_affichage():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre Affichages 
        1-) 3_6_1            4-) 3_6_4
        2-) 3_6_2            5-) 3_6_5
        3-) 3_6_3
                    
                    ''')
    
    exo = input("veuillez selectionner un numero de l'exo (ou q pour allez dans menu): ")
    if(exo == '1'):
        from chapitres.affichage.exo3_6_1 import run
        run()
    elif(exo == '2'):
        from chapitres.affichage.exo3_6_2 import run
        run()
    elif(exo == '3'):
        from chapitres.affichage.exo3_6_3 import run
        run()
    elif(exo == '4'):
        from chapitres.affichage.exo3_6_4 import run
        run()
    elif(exo == '5'):
        from chapitres.affichage.exo3_6_5 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Invalide")
        
    precedent = input(f'''
{'_'*30}

allez dans menu ? [y/n]--> ''')
    if(precedent != 'y'):
        choix_exo_affichage()
        
def choix_exo_listes():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre Listes
        --------------------
        1- 4_10_1           4- 4_10_4
        2- 4_10_2           5- 4_10_5
        3- 4_10_3           6- 4_10_6
                    
                    ''')
    
    exo = input("veuillez selectionner un l'exo (q pour allez dans menu): ")
    if(exo == '1'):
        from chapitres.liste.exo4_10_1 import run
        run()
    elif(exo == '2'):
        from chapitres.liste.exo4_10_2 import run
        run()
    elif(exo == '3'):
        from chapitres.liste.exo4_10_3 import run
        run()
    elif(exo == '4'):
        from chapitres.liste.exo4_10_4 import run
        run()
    elif(exo == '5'):
        from chapitres.liste.exo4_10_5 import run
        run()
    elif(exo == '6'):
        from chapitres.liste.exo4_10_6 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Invalide")
        
    precedent = input(f'''
{'_'*30}

allez dans menu ? [y/n]--> ''')
    if(precedent != 'y'):
        choix_exo_listes()

def choix_exo_boucles():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre Boucles
        --------------------
        1-) 5_4_1           6-) 5_4_6             11-) 5_4_11
        2-) 5_4_2           7-) 5_4_7             12-) 5_4_12
        3-) 5_4_3           8-) 5_4_8             13-) 5_4_13
        4-) 5_4_4           9-) 5_4_9             14-) 5_4_14
        5-) 5_4_5          10-) 5_4_10
                    ''')
    
    exo = input("veuillez selectionner un l'exo (ou q pour allez dans menu): ")
    if(exo == '1'):
        from chapitres.boucles.exo5_4_1 import run
        run()
    elif(exo == '2'):
        from chapitres.boucles.exo5_4_2 import run
        run()
    elif(exo == '3'):
        from chapitres.boucles.exo5_4_3 import run
        run()
    elif(exo == '4'):
        from chapitres.boucles.exo5_4_4 import run
        run()
    elif(exo == '5'):
        from chapitres.boucles.exo5_4_5 import run
        run()
    elif(exo == '6'):
        from chapitres.boucles.exo5_4_6 import run
        run()
    elif(exo == '7'):
        from chapitres.boucles.exo5_4_7 import run
        run()
    elif(exo == '8'):
        from chapitres.boucles.exo5_4_8 import run
        run()
    elif(exo == '9'):
        from chapitres.boucles.exo5_4_9 import run
        run()
    elif(exo == '10'):
        from chapitres.boucles.exo5_4_10 import run
        run()
    elif(exo == '11'):
        from chapitres.boucles.exo5_4_11 import run
        run()
    elif(exo == '12'):
        from chapitres.boucles.exo5_4_12 import run
        run()
    elif(exo == '13'):
        from chapitres.boucles.exo5_4_13 import run
        run()
    elif(exo == '14'):
        from chapitres.boucles.exo5_4_14 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Invalide")
        
    precedent = input(f'''
{'_'*30}

allez dans menu ? [y/n]--> ''')
    if(precedent != 'y'):
        choix_exo_boucles()

def choix_exo_tests():
    clear()
    pass
    print(f'''
                
    {'_'*30}
        Chapitre test
        --------------------
        1-) 6_7_1           6-) 6_7_6
        2-) 6_7_2           7-) 6_7_7
        3-) 6_7_3           8-) 6_7_8
        4-) 6_7_4           9-) 6_7_9
        5-) 6_7_5          10-) 6_7_10
                    ''')
    
    exo = input("veuillez selectionner un numero de l'exo (ou q pour allez dans menu): ")
    if(exo == '1'):
        from chapitres.tests.exo6_7_1 import run
        run()
    elif(exo == '2'):
        from chapitres.tests.exo6_7_2 import run
        run()
    elif(exo == '3'):
        from chapitres.tests.exo6_7_3 import run
        run()
    elif(exo == '4'):
        from chapitres.tests.exo6_7_4 import run
        run()
    elif(exo == '5'):
        from chapitres.tests.exo6_7_5 import run
        run()
    elif(exo == '6'):
        from chapitres.tests.exo6_7_6 import run
        run()
    elif(exo == '7'):
        from chapitres.tests.exo6_7_7 import run
        run()
    elif(exo == '8'):
        from chapitres.tests.exo6_7_8 import run
        run()
    elif(exo == '9'):
        from chapitres.tests.exo6_7_9 import run
        run()
    elif(exo == '10'):
        from chapitres.tests.exo6_7_10 import run
        run()
    elif exo == 'q':
        return
    else:
        print("Invalide")
        
    precedent = input(f'''
{'_'*30}

allez dans menu ? [y/n]--> ''')
    if(precedent != 'y'):
        choix_exo_tests()