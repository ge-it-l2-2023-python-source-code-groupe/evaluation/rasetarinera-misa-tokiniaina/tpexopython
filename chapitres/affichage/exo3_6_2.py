def run():
    print(f'''
    {'*'*30}

    #exo3_6_2

    Générez une chaîne de caractères représentant un brin d'ADN poly-A

    {'*'*30}

    ==Reponse==

    'A'*20 => {'A'*20}

    ''')

