def run():
    print(f'''
    {'*'*30}
    #exo3_6_5
 
    On souhaite que le programme affiche la sortie suivante :
    Le pourcentage de GC est 48%
    Le pourcentage de GC est 47.8%
    Le pourcentage de GC est 47.80%
    Le pourcentage de GC est 47.804 %

    {'*'*30}

    '''
    )

    perc_GC = ((4500 + 2575)/14800)*100

    print(f'''
    ===Reponse===

    Le pourcentage de GC est : {perc_GC:.0f}
    Le pourcentage de GC est : {perc_GC:.1f}
    Le pourcentage de GC est : {perc_GC:.2f}
    Le pourcentage de GC est : {perc_GC:.3f}

    '''
    )