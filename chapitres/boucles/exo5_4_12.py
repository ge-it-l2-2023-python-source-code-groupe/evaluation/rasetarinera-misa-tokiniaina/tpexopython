def run():
    print('''
        #exo5_4_12 Parcours de demi-matrice :
        ''')
    n = int(input('Entrez le rang de la matrice : '))
    count = 0

    for i in range(n):
        for j in range(i+1,n):
            print(i+1 , j+1 , sep=' ')
            count+=1

    print(f'Pour cette matrice de taille {n}*{n}: le nombre de case parcouru est : {count}')

    print('''
        Seconde version du script:
        -------------------------
        
        ''')
    for n in range(2,11):
        case_parcouru = 0
        for i in range(n):
            for j in range(i+1,n):
                case_parcouru+=1
        print(f'Pour une matrice de taille {n}*{n} : On a parcouru {case_parcouru} case(s)')