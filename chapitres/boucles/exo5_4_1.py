def run():
      print('''
            exo5_4_1
            ''')

      print('''
            premiere facon : 
            for x in ["vache", "souris", "levure", "bacterie"]:
            print(x) =>
            ''')

      for animal in ["vache", "souris", "levure", "bacterie"]:
            print(f'. {animal}')
      
      print('''
            Deuxieme facon : 
            for x in range(len(animaux))
            print(x) =>
            ''')

      animaux = ["vache", "souris", "levure", "bacterie"]
      for i in range(len(animaux)):
            print(f'. {animaux[i]}')
      
      print('''
            Troisieme fa facon : 
            i=0
      while(i < len(animaux)):
            print(animaux[i])
            i+=1
            ''')
      i=0
      while(i < len(animaux)):
            print(f'. {animaux[i]}')
            i+=1