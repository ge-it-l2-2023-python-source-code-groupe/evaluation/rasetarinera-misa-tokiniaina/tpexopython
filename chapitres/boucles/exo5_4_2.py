def run():
    print(f'''
        #exo5_4_2
        ''')

    semaine = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

    print('''
   ------for method------
    for jour in semaine[:5]:
        print(jour)
        
        ''')
    for jour in semaine[:5]:
        print(f". {jour}")


    print('''
   ------while method-----

        i=-2
        while i<0:
            print(f'. {semaine[i]}')
        ''')
    i=-2
    while i<0:
        print(f'. {semaine[i]}')
        i+=1