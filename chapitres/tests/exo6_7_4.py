def run():
      print('''
            #exo6_7_4 Fréquence des acides aminés

            ''')
      acide = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]
      print(f'''
            Reponse: 
            
            Frequence de A : {acide.count('A')} sur {len(acide)} acide amines
            
            Frequence de R : {acide.count('R')} sur {len(acide)} acide amines
            
            Frequence de G : {acide.count('G')} sur {len(acide)} acide amines
            
            Frequence de W : {acide.count('W')} sur {len(acide)} acide amines
            ''')