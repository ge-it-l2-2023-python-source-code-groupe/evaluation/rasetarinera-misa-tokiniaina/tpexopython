def run():
    print('''
        #exo6_7_2 Séquence complémentaire d'un brin d'ADN
        ''')

    adn  = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]
    print(f'adn => {adn}')


    for i in range(len(adn)):
        if adn[i] == "A":
            adn[i] = 'T'
        elif adn[i] == 'T':
            adn[i] = 'A'
        elif adn[i] == 'C':
            adn[i] = 'G'
        else:
            adn[i] = 'C'

        
    print(f'''
    complementaire => {adn}
        ''')