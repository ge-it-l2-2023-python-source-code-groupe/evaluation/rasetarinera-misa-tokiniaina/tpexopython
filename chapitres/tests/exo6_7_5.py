def run():
    print('''
        #exo6_7_5 Notes et mention d'un étudiant
        
        ''')
    notes = [14, 9, 13, 15 ,12]
 
    moyenne = (sum(notes))/len(notes)

    mention ="Bien"

    if 10<= moyenne <12:
        mention = "passable"
    elif 12<= moyenne <14:
        mention = "assez bien"

        
    print(f'''
        La note max est : {max(notes)}
        La note min est : {min(notes)}
        La moyenne est : {moyenne:.2f}
        Mention : {mention}
        ''')