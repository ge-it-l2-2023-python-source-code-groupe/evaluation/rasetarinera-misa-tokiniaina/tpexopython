def run():
  print('''
        #exo6_7_10 Recherche d'un nombre par dichotomie
        ''')
  debut = 1
  fin = 100

  print('Pensez à un nombre entre 1 et 100.')
  question = 0
  #le boucle s'arrete une fois le debut et la fin n'est plus dans le bonne ordre (chiffre trouve)
  while debut < fin:
      milieu = (debut + fin) // 2
      réponse = input(f"Le nombre se trouve entre {debut} et {fin}, Je propose {milieu} ? [+/-/=] :")
      question += 1
      if réponse == "+":
        debut = milieu + 1
      elif réponse == "-":
        fin = milieu
      else:
          print(f"J'ai trouve le nombre en {question} questions")
          break
  if debut == fin:
      print(f"J'ai trouve le nombre {fin} en {question} questions ")