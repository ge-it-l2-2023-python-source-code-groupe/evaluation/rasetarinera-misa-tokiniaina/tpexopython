def run():
    print('''
        #exo6_7_1 Jour de la semaine :
        ''')
    semaine = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

    for jour in semaine:
        if jour == 'vendredi':
            print("chouette c'est vendredi")
        elif jour == 'samedi' or jour=='dimanche':
            print(f'Repos ce week-end on est {jour}')
        else:
            print(f"Au travail on est encore {jour}")