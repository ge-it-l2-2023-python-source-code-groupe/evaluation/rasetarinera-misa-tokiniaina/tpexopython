def run():
    print(f'''
    {'_'*30}
        #exo6_7_6 Nombres pairs
    {'_'*30}
        ''')

    for i in range(21):
        if i%2 == 0 and i<=10:
            print(i)
        elif i%2 == 1 and i>10:
            print(i)