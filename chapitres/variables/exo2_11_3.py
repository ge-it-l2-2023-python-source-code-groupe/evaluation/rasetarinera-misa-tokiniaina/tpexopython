def run():
  print("""
      #exo2_11_3

  """)

  print(f"""
    Expression: str(4) * int("3")
      Prediction : "444"
      Interpreteur : {str(4) * int("3")}

        """)


  print(f"{'*'*40}")
  print(f"""

    Expression: str(4) * int("3")
      Prediction : "444"
      Interpreteur : {str(4) * int("3")}

        """)

  print(f"{'*'*40}")
  print(f"""

    Expression: int("3") + float("3.2")
      Prediction : 6.2
      Interpreteur : {int("3") + float("3.2")}

        """)

  print(f"{'*'*40}")
  print(f"""

    Expression: str(3) * float("3.2")
      Prediction : "Erreur"
      Interpreteur : can't multiply sequence by non-int of type 'float'

        """)

  print(f"{'*'*40}")
  print(f"""

    Expression: str(3/4) * 2
      Prediction : "3/43/4"
      Interpreteur : {str(3/4) * 2}

        """)