def run():
    print(f'''
    {'*'*30}
    #exo4_10_1 
    {'*'*30}
    '''
    )

    week = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

    print(f'''
    Pour recuperer les 5 premier il suffit de slice de 0 a -2
    liste[:-2] => {week[:-2]}

    En utilistant un autre indicage:
    liste[:5] => {week[:5]}

    Deux moyens pour acceder au dernier jour de la semaine:

    liste[-1] => {week[-1]}
    liste[:len(liste)-1] => {week[len(week)-1]}

    pour inverser la liste de la semaine il suffit d'utiliser le pas a -1:
    liste[::-1] => {week[::-1]}

    '''
    )