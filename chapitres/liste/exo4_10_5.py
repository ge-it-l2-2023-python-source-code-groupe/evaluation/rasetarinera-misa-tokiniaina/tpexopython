def run():
      print(f'''
            {'*'*30}
            #exo4_10_5
            {'*'*30}
            ''')

      week = ['lundi','mardi','mercredi','jeudi','vendredi','samedi','dimanche']

      print(f'''
            ===Reponse===
            
            liste de la semaine => {week}
            valeur de la semaine 4 => {week[4]}
            ''')

      week[0],week[-1]=week[-1],week[0]

      print(f'''
            (week[0],week[-1]=week[-1],week[0]) => {week}

            
      Pour afficher 12 fois la derniere valeur:
            week[-1]*12 => {week[-1]*12}
            Oui c'est lundi parcequ'on a inverse la liste
            
            {'*'*30}
            ''')