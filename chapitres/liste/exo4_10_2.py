def run():
    print(f'''
    {'*'*30}

    #exo4_10_2

    {'*'*30}
    '''
    )

    hiver = ['decembre','janvier','fevrier']
    printemps = ['mars','avril','mai']
    ete = ['juin','aout','sept']
    automne = ['oct','nov','dec']

    saisons = [hiver,  printemps, ete, automne]

    print(f'''
        saisons[2]
            Prediction: ['juin','aout','sept']
            Interpreteur : {saisons[2]}
            {'*'*30}
            
        saisons[1][0]
            Prediction: mars
            Interpreteur : {saisons[1][0]}
            {'*'*30}
            
        saisons[1:2]
            Prediction: [['mars','avril','mai'],['juin','aout','sept']]
            Interpreteur : {saisons[1:2]}
            {'*'*30}
        
        saisons[:][1]
            Prediction : ['mars','avril','mai']
            Interpreteur : {saisons[:][1]}
            
            Explication : saisons[:] renvoie saisons car on n'as pas defini ni le debut ni la fin du tranche donc [1] renvoie le tableau sur l'indice 1
            
            {'*'*30}
            
        '''
    )