def run():
      lstVide = []
      lstFlottant = [0.0]*5

      print(f'''
            {'*'*30}
            #exo4_10_6
            ''')

      print(f'''
            Reponse:
            --------
            
            affichage des listes:
            lstVide => {lstVide}
            lstFlottant => {lstFlottant}
            
            ''')

      lstVide = list(range(0,1001,200))
      print(f'''
            Ajouter à la liste lstVide les nombres entre 0 et 1000 avec « step » de 200   
            lstVide = list(range(0,1001,200)) => {lstVide}
            
            
            affichage des entiers avec la fonction range:
            
            list(range(0,4)) => {list(range(0,4))}
            [list(range(4,7)) => {list(range(4,7))}
            list(range(2,9,2)) => {list(range(2,9,2))}
            
            ''')

      lstElmnt = list(range(0,6))
      lstElmnt = lstElmnt+ lstVide + lstFlottant

      print(f'''
            lstElmnt = list(range(0,6))
            
            Ajouter le contenu des deux listes() à la fin de la liste lstElmnt.
            lstElmnt = lstElmnt+ lstVide + lstFlottant => {lstElmnt}
            ''')