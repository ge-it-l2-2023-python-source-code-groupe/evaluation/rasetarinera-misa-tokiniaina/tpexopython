from tools.console import clear
from helper import *

if __name__=="__main__":
    while True:
        clear()
        pass
        print(f'''
        {'_'*30}
        1: Variables,
        2: Affichage,
        3: Liste,
        4: Boucles et comparaisons,
        5: Test,
        Q: Quitter
        
          ''')
        
        chap = input("Veuillez choisir: ")
        clear()
        pass
        match chap:
            case '1':
                choix_exo_variables()
            case '2':
                choix_exo_affichage()
            case '3':
                choix_exo_listes()
            case '4':
                choix_exo_boucles()
            case '5':
                choix_exo_tests()
            case 'q':
                break
            case 'Q':
                break
            case _:
                print('invalide')